from json.decoder import JSONDecodeError
from driver_base import DriverBase

import signal
import time
import os
import sys
import logging
import json
from pydispatch import dispatcher

logger = logging.getLogger("WebConfigDriver")

'''
This example driver uses the web configuration to create devices and talents
Please paste this json into the Driver's devices config, you can validate it here: https://jsonlint.com/

[{
	"name": "TV",
	"description": "Demo Fake TV with Power and Volume",
	"unique_id": "Tv1,
	"talents": [{
		"name": "Power",
		"description": "Powers on the TV"
	}, {
		"name": "Volume",
		"description": "Adjust Volume Level",
		"is_range": true,
		"range_min": 0,
		"range_max": 100
	}]
}]
'''

class WebConfigDemoDriver(DriverBase):
    def __init__(self):
        self.fakeTV = None
        self.poll_fake_TV()
        self.count = 0
        super().__init__()

    def poll_fake_TV(self):
        try:
            with open('FakeTV.json', 'r') as f:
                self.fakeTV = json.load(f)
        except (JSONDecodeError, FileNotFoundError):
            # We need to create the default json file
            with open('FakeTV.json', 'w') as f:
                fake_device = {"unique_id": "Tv1",
                               "online": True,
                               "power": False,
                               "volume": 10,
                               "volume 2": 20
                               }
                json.dump(fake_device, f)
            self.fakeTV = fake_device

    def on_updated_driverconfig(self, config):
        pass


    def on_talent_change(self, talent):
        # When our talents are interacted with on_talent_change will be called by driverbase

        # Control the TV and provide feed back
        if not talent.is_range:
            if talent.set_on:
                self.talent_clear(talent=talent, clear='set_on', in_transition=True)
                # Control the TV then update the state
                current_value = self.control_tv(parameter=talent.name.lower(), value=True)
                self.talent_update(talent=talent, state=self.fakeTV[talent.name.lower()])


            if talent.set_off:
                self.talent_clear(talent=talent, clear='set_off', in_transition=True)
                # Control the TV then update the state
                self.control_tv(parameter=talent.name.lower(), value=False)
                self.talent_update(talent=talent, state=self.fakeTV[talent.name.lower()])

        if talent.is_range:

            # We need to check range
            if talent.requested_range_value != talent.range_value:
                # Control the TV then update the range
                self.control_tv(talent.name.lower(), talent.requested_range_value)
                self.talent_update(talent=talent, range_value=self.fakeTV[talent.name.lower()])



    def control_tv(self, parameter, value):

        if parameter == "power":
            # Simulate turning on
            time.sleep(.7)
        else:
            # Simulate turning off
            time.sleep(.5)

        with open('FakeTV.json', 'w') as f:
            json.dump(self.fakeTV, f)

        # Update our dictionary
        self.fakeTV[parameter] = value

    def periodic(self):
        # This will be call once a second by DriverBase

        # First try to get the web device by unique id

        web_device = self.get_device_by_unique_id("Tv1")

        # Check if it exists -- if not it probably hasn't been added to the driver configuration on the web
        if web_device:
            # Poll the device every 10 seconds
            if self.count >= 10:
                self.poll_fake_TV()
                self.count = 0
            else:
                self.count += 1
            
            # Update web with current status
            if web_device.online != self.fakeTV['online']:
                # Update to online value on the web server
                self.device_update(device=web_device, online=self.fakeTV['online'])
        else:
            logger.critical(f"Unable to find local_device Tv1, this can happen if you haven't added it to your driver config")


### Everything below here is used to run the driver and only stop when Ctrl-C is pressed, or shutdown signal is sent by the OS

class ServerExit(Exception):
    pass
    
def service_shutdown(signum, frame):
    # print('Caught signal %d' % signum)
    logger.critical("shutting down....")
    quit()

def main():
    signal.signal(signal.SIGTERM, service_shutdown)
    signal.signal(signal.SIGINT, service_shutdown)

    my_driver = WebConfigDemoDriver()
    my_driver.daemon = True
    my_driver.start()
    try:
        while True:
            time.sleep(1)
    except ServerExit:
        logger.critical('Shutting down')
        dispatcher.send(signal="Shutdown")
        my_driver.join()


if __name__ == '__main__':
    main()
