from driver_base import DriverBase

import RPi.GPIO as GPIO
import signal
import time
import os
import sys
import logging
import json
from pydispatch import dispatcher


logger = logging.getLogger("WebConfigDriver")


'''
This example driver uses the web configuration to create devices and talents
Please paste this json into the Driver's devices config, you can validate it here: https://jsonlint.com/

[{
	"name": "RaspberryPI",
	"description": "This is the Raspberry PI device",
	"unique_id": "RaspberryPI-1",
	"talents": [{
		"name": "GPIO 8",
		"description": "Controls LED connected to pin 8"
	}, {
		"name": "GPIO 10",
		"description": "Controls PWM to pin 12",
		"is_range": true,
		"range_min": 0,
		"range_max": 100
	}]
}]
'''

class WebConfigDemoDriver(DriverBase):
    def __init__(self):
        GPIO.setmode(GPIO.BOARD)
        GPIO.setup(8, GPIO.OUT)
        GPIO.setup(12, GPIO.PWM)
        self.pwm = GPIO.PWM(12, 1000)
        self.pwm.start(50)
        self.count = 0
        super().__init__()

    def poll_outputs(self):
        
        output_8_state = GPIO.input(8)
        output_12_state = GPIO.input(12)

        localdevice = self.get_device_by_unique_id('RaspberryPI-1')    
        talent_output8 = localdevice.get_talent_by_name(name="GPIO 8")
        talent_output12 = localdevice.get_talent_by_name(name="GPIO 12")
        
        if talent_output8.state != output_8_state:
            self.talent_update(talent=talent_output8, state=output_8_state)

        if talent_output12.state != output_12_state:
            self.talent_update(talent=talent_output12, range_value=output_12_state)

    def on_updated_driverconfig(self, config):
        pass


    def on_talent_change(self, talent):
        # When our talents are interacted with on_talent_change will be called by driverbase

        # Control the TV and provide feed back
        if not talent.is_range:
            if talent.set_on:
                self.talent_clear(talent=talent, clear='set_on', in_transition=True)
                # Control the TV then update the state
                self.control_gpio(parameter=talent.name, value=True)
                self.talent_update(talent=talent, state=True)


            if talent.set_off:
                self.talent_clear(talent=talent, clear='set_off', in_transition=True)
                # Control the TV then update the state
                self.control_gpio(parameter=talent.name, value=False)
                self.talent_update(talent=talent, state=False)

        if talent.is_range:

            # We need to check range
            if talent.requested_range_value != talent.range_value:
                # Control the TV then update the range
                self.control_gpio(talent.name, talent.requested_range_value)
                self.talent_update(talent=talent, range_value=talent.requested_range_value)


    def control_gpio(self, parameter, value):
        if parameter == "GPIO 8":
            if value:
                GPIO.output(8, GPIO.HIGH)
            else:
                GPIO.output(8, GPIO.LOW)
        if parameter == "GPIO 12":
            if 0 <= value <= 100: 
                self.pwm.ChangeDutyCycle(value)
            else:
                logger.critical(f"Invalid DutyCycle: {value}")
        
    def periodic(self):
        # Periodic will be call once a second by DriverBase
        self.count += 1
        # Poll the device every 10 seconds
        if self.count >= 10:
            # First try to get the web device by unique id
            web_device = self.get_device_by_unique_id('RaspberryPI-1')

            # Check if it has been added to the driver configuration on the web
            if web_device:
                self.poll_()
            self.count = 0
                
            # Update web with current status
            if not web_device.online:
                # Update to online value on the web server
                self.device_update(device=web_device, online=True)


### Everything below here is used to run the driver and only stop when Ctrl-C is pressed, or shutdown signal is sent by the OS

class ServerExit(Exception):
    pass
    
def service_shutdown(signum, frame):
    # print('Caught signal %d' % signum)
    logger.critical("shutting down....")
    quit()

def main():
    signal.signal(signal.SIGTERM, service_shutdown)
    signal.signal(signal.SIGINT, service_shutdown)

    my_driver = WebConfigDemoDriver()
    my_driver.daemon = True
    my_driver.start()
    try:
        while True:
            time.sleep(1)
    except ServerExit:
        logger.critical('Shutting down')
        dispatcher.send(signal="Shutdown")
        my_driver.join()


if __name__ == '__main__':
    main()
