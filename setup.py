import setuptools

with open("README.md", "r", encoding="utf-8") as fh:
    long_description = fh.read()

setuptools.setup(
    name="driver-base",
    version="0.2.6",
    author="Jim Maciejewski",
    author_email="magicsoftware@ornear.com",
    description="Driver Base",
    long_description=long_description,
    long_description_content_type="text/markdown",
    url="https://bitbucket.org/itsmagic/driver_base",
    packages=setuptools.find_packages(),
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: MIT License",
        "Operating System :: OS Independent",
    ],
    install_requires=['websocket-client', 'pydispatcher', 'cryptography==3.3.2'],
    python_requires='>=3.6',
)