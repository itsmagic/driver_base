# OpenAV Driver Base -- _python drivers made easy_

## Intent
To make creating drivers for [OpenAV](https://bitbucket.org/itsmagic/openav) systems as easy as possible in python

### Concepts

### Driver Management
1. Manual management 
    * The driver is responsible for configuration
        * Pros:
            * Simplicity -- all changes are handled in the drivers code
        * Cons:
            * Any changes have to be made in the driver code, only a programmer could make them

2. Driver Config 
    * The driver uses the web based driver configuration 
        * Pros: 
            * Allows configuration changes to happen in realtime
            * No programming necessary
        * Cons:
            * More complex driver programming

### Maintaining state
* All state is stored in the OpenAV Django database
* We need to maintain a local mirror of this state to minimize websocket requests
* Using the _Driver Base_ gives you access to this state using the following API

    * get_device_by_unique_id(uuid)
        * this function returns an _Device_ object 
            * Device has the following attributes
            * online
    

    * Using the _Device_ object above you can access it's talents
        ```
        localdevice = self.get_device_by_unique_id('pool pump')
        talent = localdevice.get_talent_by_name(name="Pump Power")
        self.talent_update(talent=talent, state=True)
        ```

        


To create a service for your driver 



```
[Unit]
Description=Demo Driver service for openav
After=network.target
After=openav.service

[Service]
Restart=on-failure
User=**USER NAME HERE**
Environment=SERVER_URL=http://localhost
Environment=WS_TOKEN=**ADD_TOKEN_HERE**
Environment=DRIVER_NAME=Demo Driver
#Environment=LOG_LEVEL=10
WorkingDirectory=/home/**USER NAME HERE**/drivers/demo_driver/source/examples/WebConfigDriver
ExecStart=/home/**USER NAME HERE**/drivers/demo_driver/virt_env/bin/python /home/**USER NAME HERE**/drivers/demo_driver/source/examples/WebConfigDriver/demodriver.py

[Install]
WantedBy=multi-user.target
```


### Example steps

1. Install requirements

    ```
    pip install pydispatcher 
    pip install -U git+https://bitbucket.org/itsmagic/driver_base.git
    ```

2. Create a driver file that subclasses driverbase, in this example the file is called example_driver.py

    ```
    from driver_base import DriverBase

    import logging
    import signal
    import time
    from pydispatch import dispatcher


    class WebConfigDemoDriver(DriverBase):
        def __init__(self):
            super().__init__()

        def on_talent_change(self, talent):
            # When our talents are interacted with on_talent_change will be called by driverbase
            pass
            
        def periodic(self):
            # This will be call once a second by DriverBase
            pass

        def on_updated_driverconfig(self, config):
            """ Since we are doing the driver configuration in the web, we just pass here
            """
            pass




    ### Everything below here is used to run the driver and only stop when Ctrl-C is pressed, or shutdown signal is sent by the OS

    class ServerExit(Exception):
        pass
        
    def service_shutdown(signum, frame):
        # print('Caught signal %d' % signum)
        logger.critical("shutting down....")
        quit()

    def main():
        signal.signal(signal.SIGTERM, service_shutdown)
        signal.signal(signal.SIGINT, service_shutdown)

        my_driver = WebConfigDemoDriver()
        my_driver.daemon = True
        my_driver.start()
        try:
            while True:
                time.sleep(1)
        except ServerExit:
            logger.critical('Shutting down')
            dispatcher.send(signal="Shutdown")
            my_driver.join()


    if __name__ == '__main__':
        main()
    ```

3. We need to set the following environment variables WS_TOKEN, DRIVER_NAME, SERVER_URL and optionally LOG_LEVEL. 
    
    * WS_TOKEN - needs to be created on the the OpenAV server admin pages, if  you create a new user a token will be automatically be generated. 
    * DRIVER_NAME - This is the name the driver will be refereed to on the admin pages
    * SERVER_URL - This is the servers url. 
    * LOG_LEVEL - This is the local logging level. This will be deprecated soon, as it can be set in the admin pages.  10=DEBUG 50=CRITICAL

    Windows Powershell
    ```
    PS C:\> $ENV:WS_TOKEN="a57a98a3d6ee96d7bea77888568989cfeb495733"
    PS C:\> $ENV:DRIVER_NAME="Example Driver"
    PS C:\> $ENV:SERVER_URL="https://openav.ornear.com"
    PS C:\> $ENV:LOG_LEVEL="50"

    To verify, just echo the variable:

    PS C:\> echo $ENV:DRIVER_NAME
    Example Driver
    PS C:\>
    ```

    Linux
    ```
    export WS_TOKEN="a57a98a3d6ee96d7bea77888568989cfeb495733"
    export DRIVER_NAME="Example Driver"
    export SERVER_URL="https://openav.ornear.com"
    export LOG_LEVEL="50"

    To verify, just echo the variable:

    pi@raspberry:~ $ echo $DRIVER_NAME
    Example Driver
    pi@raspberry:~ $

    ```

4. Start the driver 

    ```
    pi@raspberry:~ $ python example_driver.py
    08:39:24 CRITICAL:DriverRegistration: Driver has been created, please authorize this driver in the web ui to continue
    08:39:25 CRITICAL:DriverRegistration: Driver is not authorized...
    08:39:35 CRITICAL:DriverRegistration: Driver is not authorized...
    ...
    ```

5. As the log says, we need to authorize the driver in the web ui
    On the web ui browse to https://openav.ornear.com/admin/plankowner/driver/
    Click on "Example Driver" and enable the "Authorized" check box. Then click save.

6. After the next poll (10 seconds) the driver should be authorized, which will allow it to create a blank Driver Config

    ```
    pi@raspberry:~ $ python example_driver.py
    08:39:24 CRITICAL:DriverRegistration: Driver has been created, please authorize this driver in the web ui to continue
    08:39:25 CRITICAL:DriverRegistration: Driver is not authorized...
    08:39:35 CRITICAL:DriverRegistration: Driver is not authorized...
    08:39:45 CRITICAL:DriverRegistration: Driver is not authorized...
    08:41:55 CRITICAL:DriverRegistration: Driver is not authorized...
    08:42:05 CRITICAL:DriverRegistration: Driver is not authorized...
    08:42:18 CRITICAL:DriverBase: Driver Config has been created
    ```
7. We can now edit the Driver Config to add Devices and Talents to this driver

    * The configuration is represented in a JSON list
    * Each item in this list is a Device and requires the following
        * name - The name of the device
        * description - A long description of the device
        * unique_id - A unique identifier, this must be unique in the entire system 
        * talents - A list of talents with the following attributes
            * name - This is the name that will show on the talent button
            * description - A long description of the Talent
        * If the talent acts as a range, it will require additional attributes
            * is_range - needs to be set true
            * range_min - Lowest value for range
            * range_max - Highest value for range

    * In the admin pages, add the following to the Example Driver under Driver Config, and press save
    ```
    [{
        "name": "RaspberryPI",
        "description": "This is the Raspberry PI device",
        "unique_id": "RaspberryPI-1",
        "talents": [{
            "name": "Power",
            "description": "Turns the power on"
        }, {
            "name": "Volume",
            "description": "Sets the volume level",
            "is_range": true,
            "range_min": 0,
            "range_max": 100
            }]
    }]
    ```

    * Once saved, a Device named RaspberryPI, a Talent named Power, and a Talent named Volume will be created. This can be verified in the Devices and Talents sections of the admin pages. 

8. Getting our device marked "online"
    * If you look at the RaspberryPI device, you will note that it is currently marked offline. It is the our drivers responsibility to mark devices on and off line
    * To correct this we will need to add code to the driver to mark it online
    * Since this driver is running on the RaspberryPI device, we will always mark it online
    * We will use two functions we inherited from DriverBase
        * self.get_device_by_unique_id
            * This will get the Device object using our device unique_id 
            * We need this Device object to make changes to it
        * self.device_update 
            * We use this to change the online status of a Device
    * Add the following code to example_driver.py in the periodic function
    ``` 
    ...

    def periodic(self):
        # This will be call once a second by DriverBase

        # We should check that we are marked online
        # First try to get the web device by unique id
        web_device = self.get_device_by_unique_id('RaspberryPI-1')

        # Check if it has been added to the driver configuration on the web
        if web_device:
            # Update web with current status
            if not web_device.online:
                # Update to online value on the web server
                self.device_update(device=web_device, online=True)
    ...

    ```
    * Save and restart the driver, if it is working there will be no output
    ```
    pi@raspberry:~ $ python example_driver.py

    ```
    * You should now be able to go to the admin pages and see the RaspberryPI device is marked online
9. Create a Panel Style for our talents
    * Click the +Add button on Panel Styles in the admin pages
    * Name the panel "Example Panel"
    * In Talent order put 0
    * Save the panel
    * Reopen the panel style by clicking on Example Panel
    * Get the panels ID from the URL for this example we are on 4:
        * https://openav.ornear.com/admin/plankowner/panelstyle/4/change/

    * Go to the button arrangement page here, substituting your panelstyle id
        * https://.ornear.com/ng-app/config/4
        * Drag and drop the Power talent into the Panel column

    * Go to the panel page
        * https://openav.ornear.com/panel/4

10. Talent behavior 
    * When you click the Power button the following events occur
        * The webpage sends a websocket message to the web server for that talent set_on = True
        * The server makes this change and the driver is notified 
        * The DriverBase calls our on_talent_change function with the Talent object as an argument
    * It is our drivers responsibility to acknowledge the set_on and clear it
        * If our driver doesn't clear the set_on, our driver will be marked unresponsive and all its talents will be unavailable (marked grey)

11. Having the Talent respond to changes
    * We need to do the following when a talent changes
        * Clear the set_on or set_off, and set in_transition=True
        * Do the actual action (Turn power on or off)
        * Once the action is complete, clear in_transition and update the state of the talent
    * Add the following to the our on_talent_change function
    ```
    if talent.set_on:
        self.talent_clear(talent=talent, clear='set_on', in_transition=True)
        
        # Actually do the power ON here

        self.talent_update(talent=talent, state=True)


    if talent.set_off:
        self.talent_clear(talent=talent, clear='set_off', in_transition=True)
        
        # Actually do the power OFF here
        
        self.talent_update(talent=talent, state=False)

    ```
    * Save and restart the driver
    * Our driver will connect and clear the unresponsive status
    * The Power button will now turn on and off if you click it
