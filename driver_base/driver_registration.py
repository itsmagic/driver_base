import driver_base
from driver_base.reconnecting_websocket import ReconnectingWebsocket
from pydispatch import dispatcher
import json
from threading import Thread
import time
import logging

logger = logging.getLogger("DriverRegistration")

class DriverRegistration(Thread):
    """The Driver Registration Thread"""

    def __init__(self, conf=None):
        """Init the worker class"""
        self.shutdown = False
        self.conf = conf
        self.ws_connected = False
        self.authorized = False

        dispatcher.connect(self.shutdown_signal, signal="Shutdown")
        dispatcher.connect(self.ws_incoming, signal="Websocket Incoming")
        dispatcher.connect(self.ws_status, signal="Websocket Status")

        Thread.__init__(self, name="DriverRegistration")

    def shutdown_signal(self, sender):
        """Shutsdown the thread"""
        self.shutdown = True

    def start_ws(self):
        self.ws = Thread(target=ReconnectingWebsocket, kwargs={'server_url': self.conf.server_url,
                                                               'token': self.conf.get_token()})
        self.ws.daemon = True
        self.ws.start()

    def ws_incoming(self, message):
        # logger.debug(f"incoming: {message}")

        all_data = json.loads(message)
        ws_stream = all_data['stream']
        ws_payload = all_data['payload']
        if 'action' in all_data['payload']:
            ws_action = all_data['payload']['action']

        if ws_stream == "driverstream":
            driver = ws_payload['data']
            if ws_action == "create":
                if ws_payload['errors'] == ['You do not have permission to perform this action.']:
                    logger.critical(f"Your WS_TOKEN is invalid... quitting")
                    dispatcher.send(signal="Shutdown")
                    return
                self.conf.uuid = driver['driver_uuid']
                self.shelve_send(key="uuid", data=self.conf.uuid)
                self.conf.driver_id = int(driver['id'])
                self.shelve_send(key="driver_id", data=self.conf.driver_id)
                logger.critical(f"Driver has been created, please authorize this driver in the web ui to continue")

            if ws_action == "retrieve":
                if ws_payload['response_status'] != 200:
                    logger.critical(f"Unable to find this UUID / device ID shutting down, this means the server has no record of this drivers assigned UUID and you will need to delete the local shelve data, to re-register")
                    dispatcher.send(signal="Shutdown")
                    return
                if driver['driver_uuid'] == self.conf.uuid:
                    if driver['name'] != self.conf.driver_name:
                        logger.warning(f"Our driver name has changed from {driver['name']} to {self.conf.driver_name} updating the web")
                        self.ws_send(stream='driverstream', payload={"action": "patch", "data": {"name": self.conf.driver_name}, "pk": driver['id'], "request_id": 42})
                    # we have a match continue
                    self.authorized = driver['authorized']
                    if self.authorized:
                        logger.info(f"Driver is authorized...")
                    else:
                        logger.critical(f"Driver is not authorized...")

    def ws_status(self, status):
        # logger.info(f"ws status: {status}")
        if status['connected']:
            logger.info(f'Websocket connected')
            self.ws_connected = True
            if hasattr(self.conf, 'driver_id') and self.conf.driver_id is not None and self.authorized:
                self.ws_send(stream='watchdogstream', payload={"action": "update_watchdog", 'data': {}, "driver": self.conf.driver_id, "request_id": 42})
        else:
            logger.info(f'Websocket disconnected')
            self.ws_connected = False

    def run(self):
        """Run Worker Thread."""
        self.start_ws()
        driver_create_requested = False
        while not self.shutdown:

            if not self.ws_connected:
                time.sleep(1)
                logger.info('waiting for websocket connection...')
                continue
            if self.conf.driver_id is None:
                # We have never registered...
                if not driver_create_requested:
                    self.ws_send(stream='driverstream', payload={'action': 'create',
                                                                 'data': {'owner': {'id': 0},
                                                                          'name': self.conf.driver_name,
                                                                          'is_online': True,
                                                                          'devices': [],
                                                                          'configs': []},
                                                                 'request_id': 42})
                    driver_create_requested = True
                    continue
                else:
                    logger.info('Waiting for driver id')
                    time.sleep(1)
                    continue

            if not self.authorized:
                logger.warning('Verifying Authorization...')
                self.ws_send(stream='driverstream', payload={"action": "retrieve", "request_id": 42, "pk": self.conf.driver_id})
                self.wait_with_shutdown_check(10)
                continue
            else:
                # update watch_dog
                if self.ws_connected:
                    self.ws_send(stream='watchdogstream', payload={"action": "update_watchdog", 'data': {}, "driver": self.conf.driver_id, "request_id": 42})
                self.wait_with_shutdown_check(60)
                continue

            logger.critical('should never make it here')
            quit()

    def wait_with_shutdown_check(self, delay):
        """This will sleep the thread, but check shutdown every second"""
        for i in range(delay):
            if not self.shutdown:
                time.sleep(1)
            else:
                return

    def ws_send(self, stream, payload):
        if self.ws_connected:
            dispatcher.send("Websocket Send", sender=self, stream=stream, payload=payload)
        else:
            logger.info('Unable to send when disconnected')

    def shelve_send(self, key, data):
        dispatcher.send(signal="Save Shelve", key=key, data=data)


def main():
    pass


if __name__ == '__main__':
    main()
