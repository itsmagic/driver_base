from websocket import WebSocketApp
import json
from pydispatch import dispatcher
import time
import logging
from urllib.parse import urlparse

logger = logging.getLogger("ReconnectingWebsocket")


class ReconnectingWebsocket(WebSocketApp):
    def __init__(self, server_url, token, reconnect_interval=5):
        url_parts = urlparse(server_url)
        server_protocol = 'ws' if url_parts.scheme == 'http' else 'wss'
        url = f'{server_protocol}://{url_parts.netloc}/ws/multi/'
        header = [f'Authorization: Token {token}']

        on_open = lambda ws: self.ws_open(ws)  # noqa E731
        on_message = lambda ws, msg: self.ws_message(ws, msg)  # noqa E731
        on_error = lambda ws, error: self.ws_error(ws, error)  # noqa E731
        on_close = lambda ws, close_status_code, close_msg: self.ws_close(ws, close_status_code, close_msg)  # noqa E731

        self.connected = False
        self.reconnect_interval = reconnect_interval
        self.shutdown = False
        dispatcher.connect(self.shutdown_signal,
                           signal="Shutdown",
                           sender=dispatcher.Any)
        dispatcher.connect(self.send_data,
                           signal="Websocket Send",
                           sender=dispatcher.Any)
        super().__init__(url, header, on_open, on_message, on_error, on_close)
        self.run()

    def run(self):
        if self.run_forever():
            # We won't get here until the websock is disconnected
            time.sleep(5)
            self.run()
            logger.critical('Past self.run, we should restart run_forever now.')
        else:
            logger.info('Got keyboard interrupt in reconnecting websocket, closing')
            self.run()

    def shutdown_signal(self):
        logger.info("In shutdown")
        self.shutdown = True

    def send_data(self, sender, stream, payload):
        logger.debug(f"Sending data\nstream: {stream}\npayload: {json.dumps(payload, indent=4, sort_keys=True)}\nfor: {sender}")
        # payload {"action": 'subscribe_instance', "request_id": 42, "pk": 7}
        self.send(json.dumps({'stream': stream, 'payload': payload}))

    def ws_open(self, ws):
        logging.warning("Websocket connected")
        dispatcher.send(signal="Websocket Status", status={"connected": True})
        self.connected = True

    def ws_close(self, ws, status, message):
        logging.warning(f"Websocket disconnected status:{status}, message:{message}")
        dispatcher.send(signal="Websocket Status", status={"connected": False})
        self.connected = False

    def ws_message(self, ws, message):
        # logger.info('Received data: ', message)
        dispatcher.send(signal="Websocket Incoming", message=message)

    def ws_error(self, ws, exception):
        logging.critical(f"Websocket had an error {exception}")


def incoming(message):
    logger.info(message)


def status_message(status):
    logger.info(status)
