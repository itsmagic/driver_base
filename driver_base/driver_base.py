
from driver_base.driver_registration import DriverRegistration
from driver_base import datastore
import time
from threading import Thread
from pydispatch import dispatcher
import json

import os
import sys
import logging
import shelve
import signal

logging.basicConfig(
    format="%(asctime)s %(levelname)s:%(name)s: %(message)s",
    level=int(os.environ.get('LOG_LEVEL', 50)),
    datefmt="%H:%M:%S",
    stream=sys.stderr)

logger = logging.getLogger("DriverBase")
logging.getLogger("chardet.charsetprober").disabled = True


class VideoInput:
    def __init__(self, config):
        self.name = None
        self.id = None
        self.number = None
        self.description = None
        for key in config:
            if key == 'device':
                if config['device'] is not None:
                    self.device = Device(config[key])
            else:
                setattr(self, key, config[key])

    def __str__(self):
        return f"VideoInput: {self.name}"

    def __repr__(self):
        return f"VideoInput: {self.name}"


class VideoOutput:
    def __init__(self, config):
        self.name = None
        self.id = None
        self.number = None
        self.description = None
        for key in config:
            if key == 'device':
                if config['device'] is not None:
                    self.device = Device(config[key])
            else:
                setattr(self, key, config[key])

    def __str__(self):
        return f"VideoOutput: {self.name}"

    def __repr__(self):
        return f"VideoOutput: {self.name}"


class VideoRoute:
    def __init__(self, config):
        self.name = None
        self.id = None
        self.videoinput = None
        self.videooutputs = []
        self.talent = None
        self.description = None
        for key in config:
            if key == 'videoinput':
                if config['videoinput'] is not None:
                    self.videoinput = VideoInput(config['videoinput'])
            elif key == 'videooutputs':
                if config['videooutputs'] is None:
                    continue
                for videooutput_conf in config['videooutputs']:
                    self.videooutputs.append(VideoOutput(videooutput_conf))
            elif key == 'talent':
                if config['talent'] is not None:
                    self.talent = Talent(config['talent'])
            else:
                setattr(self, key, config[key])

    def __str__(self):
        return f"VideoRoute: {self.name}"

    def __repr__(self):
        return f"VideoRoute: {self.name}"


class Talent:
    def __init__(self, config):
        self.name = None
        self.description = None
        self.id = None
        self.state = False
        self.range_value = 0
        self.requested_range_value = 0
        self.is_range = False
        self.range_min = 0
        self.range_max = 0
        self.disable_if_in_transition = False
        self.is_a_route = False

        for key in config:
            if key == 'device':
                if config['device'] is not None:
                    self.device = Device(config[key])
            else:
                setattr(self, key, config[key])

    def __str__(self):
        return f"Talent: {self.name}"

    def __repr__(self):
        return f"Talent: {self.name}"


class Device:
    def __init__(self, config):

        self.name = None
        self.unique_id = None
        self.description = None
        self.id = None
        self.online = False
        self.talents = []
        self.videoinputs = []
        self.videooutputs = []

        for key in config:
            if key == 'talents':
                for talent_conf in config['talents']:
                    self.talents.append(Talent(talent_conf))
            elif key == 'videoinputs':
                for videoinput_conf in config['videoinputs']:
                    self.videoinputs.append(VideoInput(videoinput_conf))
            elif key == 'videooutputs':
                for videooutput_conf in config['videooutputs']:
                    self.videooutputs.append(VideoOutput(videooutput_conf))
            elif key == 'driver':
                self.driver = Driver(config[key])
            else:
                # logger.debug(f"Setting key: {key}")
                setattr(self, key, config[key])

    def get_talent_by_name(self, name):
        for talent in self.talents:
            if talent.name == name:
                return talent
        logger.debug(f"{self}: Unable to get talent by name: {name}")
        return None

    def get_videoinput_by_name(self, name):
        for videoinput in self.videoinputs:
            if videoinput.name == name:
                return videoinput
        logger.debug(f"{self}: Unable to get videoinput by name: {name}")
        return None

    def get_videooutput_by_name(self, name):
        for videooutput in self.videooutputs:
            if videooutput.name == name:
                return videooutput
        logger.debug(f"{self}: Unable to get videooutput by name: {name}")
        return None

    def get_videoinput_by_id(self, id):
        for videoinput in self.videoinputs:
            if str(videoinput.id) == str(id):
                return videoinput
        logger.debug(f"{self}: Unable to get videoinput by id: {id}")
        return None

    def get_videooutput_by_id(self, id):
        for videooutput in self.videooutputs:
            if str(videooutput.id) == str(id):
                return videooutput
        logger.debug(f"{self}: Unable to get videooutput by id: {id}")
        return None

    def get_talent_by_id(self, id):
        for talent in self.talents:
            if str(talent.id) == str(id):
                return talent
        # logger.debug(f"{self}: Unable to get talent by id: {id}")
        return None

    def __str__(self):
        return f"Device: {self.name}"

    def __repr__(self):
        return f"Device: {self.name}"


class Driver:
    def __init__(self, config):
        self.name = None
        for key in config:
            setattr(self, key, config[key])

    def __str__(self):
        return f"Driver: {self.name}"

    def __repr__(self):
        return f"Driver: {self.name}"


class DriverBase(Thread):
    """The Default Driver Thread"""

    def __init__(self):
        """Init the worker class"""
        self.shutdown = False
        self.ws_connected = False
        self.videoroute_monitoring = False
        self.subscriptions = {'devicestream': set(),
                              'talentstream': set(),
                              'driverconfigstream': set(),
                              'videoinputstream': set(),
                              'videooutputstream': set()}
        self.request_id_dict = {}
        self.routes = {}
        self.request_id = 1

        self._restore_config()
        # At startup, mark all devices offline
        for device in self.driverbaseconf.devices:
            device.online = False

        dispatcher.connect(self.shutdown_signal, signal="Shutdown")
        dispatcher.connect(self._ws_incoming, signal="Websocket Incoming")
        dispatcher.connect(self._ws_status, signal="Websocket Status")
        dispatcher.connect(self._save_shelve, signal="Save Shelve")
        dispatcher.connect(self._log_level, signal="LogLevel")

        Thread.__init__(self, name="DriverBase")

    def shutdown_signal(self, sender):
        """Shutsdown the thread"""
        self.shutdown = True
        os.kill(os.getpid(), signal.SIGTERM)

    def _log_level(self, level):
        logging.getLogger().setLevel(level)
        logger.critical(f"Log Level is now {level}")

    def _restore_config(self):
        env_set = True
        for item in ['WS_TOKEN', 'DRIVER_NAME']:
            if item not in os.environ:
                print(f"Please set {item} environment variable")
                env_set = False

        if not env_set:
            quit()
        for item in ['LOG_LEVEL', 'SERVER_URL']:
            if item not in os.environ:
                print(f"Environment variable not set, using defaults for {item}")
        if 'FORCE_UUID_AND_ID' in os.environ:
            force_uuid, id = os.environ['FORCE_UUID_AND_ID'].split(" ")
            id_str = " " * (len(id) + 32)
            print(f"*****          !!!WARNING!!!             *******")
            print(f"*****FORCE_UUID_AND_ID is set            *******")
            print(f"*****Setting Driver UUID to              *******")
            print(f"*****{force_uuid}*******")
            print(f"*****Setting Driver ID to                *******")
            print(f"*****{id}{id_str}*******")
            print(f"************************************************")

        self.driverbaseconf = datastore.DriverBaseConfig()

        # Setting system to demo allows the docker to work
        # self.driverbaseconf.demo = os.environ.get("DEMO", False)

        self.driverbaseconf.driver_name = str(os.environ['DRIVER_NAME'])

        # Get configured persist path
        # self.driverbaseconf.persist_path = os.environ.get('PERSIST_PATH', 'persistent_data')

        # Check if we are running in demo mode
        # if self.driverbaseconf.demo:
        #     # Make sure to store our persistent data locally
        #     self.driverbaseconf.persist_path = os.path.join("persistent_data", "".join(self.driverbaseconf.driver_name.split()))

        # Make sure path is created
        # if not os.path.exists(self.driverbaseconf.persist_path):
        #     os.makedirs(self.driverbaseconf.persist_path)

        with shelve.open('shelve_data') as db:
            # Delete stored data if reset is True
            if os.environ.get("RESET", 'False').lower() in ('true', '1', 't'):
                db.clear()
                logger.critical("RESET=TRUE, Local data has been deleted, set env RESET to False to continue")
                quit()
            else:
                all_keys = list(db.keys())
                for key in all_keys:
                    setattr(self.driverbaseconf, key, db[key])

        self.driverbaseconf.set_token(os.environ['WS_TOKEN'])
        # only use if the shelve database for this driver is lost.
        if 'FORCE_UUID_AND_ID' in os.environ:
            self.driverbaseconf.uuid, self.driverbaseconf.driver_id = os.environ['FORCE_UUID_AND_ID'].split(" ")
        # Using get will return the env value or leave it the default "localhost"
        self.driverbaseconf.server_url = os.environ.get('SERVER_URL', self.driverbaseconf.server_url)

    def configure_conf(self):
        logger.critical(f"Please override configure_conf in your driver")

    def _get_request_id(self):
        self.request_id += 1
        return self.request_id

    def _start_registration(self):
        self.registration = DriverRegistration(self.driverbaseconf)
        self.registration.daemon = True
        self.registration.start()

    def _save_shelve(self, key, data):
        # logger.debug(f"Saving shelve key: {key} as {data}")
        with shelve.open('shelve_data') as db:
            db[key] = data

    def device_update(self, device, online):
        localdevice = self.get_device_by_unique_id(device.unique_id)
        if localdevice:
            if localdevice.id is None:
                logger.debug(f"Unable to update {localdevice} when it has no id")
            else:
                self._ws_send(stream="devicestream",
                              payload={"action": "patch",
                                       "data": {'is_online': online},
                                       "pk": localdevice.id,
                                       "request_id": f"T{localdevice.id}"})
        else:
            logger.critical(f"Unable to find device by unique id {device.unique_id}")

    def talent_update(self, talent, state=False, in_transition=False, range_value=0):
        if talent.id is None:
            logger.debug(f"Unable to update {talent} when it has no id")
        else:
            self._ws_send(stream="talentstream",
                          payload={"action": "patch",
                                   "data": {"state": state,
                                            "set_on": False,
                                            "set_off": False,
                                            "in_transition": in_transition,
                                            "range_value": range_value},
                                   "pk": talent.id,
                                   "request_id": f"T{talent.id}"})

    def talent_clear(self, talent, clear, in_transition=False):
        if clear not in ['set_on', 'set_off']:
            logger.critical(f"talent_clear can only clear set_on or set_off")
            return
        localtalent = self.get_local_talent(talent=talent)
        if talent.id is None:
            logger.debug(f"Unable to update {localtalent} when it has no id")
        else:
            self._ws_send(stream="talentstream",
                          payload={"action": "patch",
                                   "data": {"set_on": False,
                                            "set_off": False,
                                            "in_transition": in_transition},
                                   "pk": talent.id,
                                   "request_id": f"T{talent.id}"})

    def select_route(self, talent):
        # A talent that is associated with a route is being executed, the user has sent the talent through
        # Driverbase needs to return the videoinput, and videooutputs involved
        # used_inputs = []

        # Since routes are mutually exclusive
        # We need to turn off any routes that are not selected
        for route in self.routes:
            if self.routes[route].talent.id == talent.id:
                # Set this talent to on
                # used_inputs.append(self.routes[route].videoinput)
                self.on_videoroute_select(self.routes[route].videoinput, self.routes[route].videooutputs, talent)
            else:
                # Set these talents to off
                self.talent_update(talent=self.routes[route].talent, state=False)

    def videoinput_update(self, videoinput, number):
        # This gets stored locally on the UPDATE event

        if videoinput.id is None:
            logger.debug(f"Unable to update {videoinput} when it has no id")
        else:
            self._ws_send(stream="videoinputstream",
                          payload={"action": "patch",
                                   "data": {"number": number},
                                   "pk": videoinput.id,
                                   "request_id": self._get_request_id()})

    def videooutput_update(self, videooutput, number):
        # This gets stored locally on the UPDATE event
        if videooutput.id is None:
            logger.debug(f"Unable to update {videooutput} when it has no id")
        else:
            self._ws_send(stream="videooutputstream",
                          payload={"action": "patch",
                                   "data": {"number": number},
                                   "pk": videooutput.id,
                                   "request_id": self._get_request_id()})

    def _ws_incoming(self, message):
        if True:
            # logger.debug(f"incoming: {message}")
            all_data = json.loads(message)
            ws_stream = all_data['stream']
            ws_payload = all_data['payload']
            ws_action = None
            if 'action' in all_data['payload']:
                ws_action = all_data['payload']['action']
            if 'errors' in ws_payload:
                if len(ws_payload['errors']) > 0:
                    logger.critical(f"WS payload contains errors: {ws_payload['errors']}")
                    # return
            # The next command will call functions for each stream so they can be overridden. It would be the same as:
            # self.on_devicestream(ws_payload, ws_action)

            getattr(self, f"on_{ws_stream}")(ws_payload, ws_action)

        # except Exception as error:
        #     logger.critical(f"Had an error processing ws message: {repr(error)} {message}")

    def on_watchdogstream(self, ws_payload, ws_action):
        # Check if the logging level has changed
        if ws_action == 'update_watchdog':
            if 'log_level' in ws_payload['data']:
                level = int(ws_payload['data']['log_level'])
                if level >= 0 and level <= 50 and level != logger.getEffectiveLevel():
                    logger.critical(f"Sending loglevel to: {level} from {logger.getEffectiveLevel()}")
                    dispatcher.send(signal="LogLevel", level=level)

    def on_driverconfig_set(self, config):
        # logger.critical(f"Please override on_driverconfig_set in your driver class")
        pass

    def on_scheduledeventstream(self, ws_payload, ws_action):
        # logger.critical(f"Please override on_scheduledeventstream in your driver class")
        pass

    def on_macrostream(self, ws_payload, ws_action):
        # logger.critical(f"Please override on_macrostream in your driver class")
        pass

    def on_panelstylestream(self, ws_payload, ws_action):
        # logger.critical(f"Please override on_panelstylestream in your driver class")
        pass

    def on_driverstream(self, ws_payload, ws_action):
        # logger.critical(f"Please override on_driverstream in your driver class: {ws_payload}")
        pass

    def on_updated_driverconfig(self, config):
        logger.critical(f"Please override on_updated_driverconfig in your driver class")

    def on_talent_change(self, talent):
        logger.critical(f"Please override on_talent_change in your driver class")

    def on_videoroute_select(self, videoinput, videooutputs, talent):
        logger.critical(f"Please override on_videoroute_select in your driver class")

    def on_videoroute_clear(self, videoinput, videooutputs, talent):
        logger.critical(f"Please override on_videoroute_clear in your driver class")

    def on_videoroutestream(self, ws_payload, ws_action):

        if ws_action == 'subscribe_instance':
            return

        if ws_action == "list":
            # If we find a route that has one of our TX and doesn't have a talent create it.
            # Initially we find these during startup
            # After that we will be subscribed to the model and get all updates
            for videoroute in [VideoRoute(r) for r in ws_payload['data']]:
                if videoroute.videoinput is None or videoroute.videooutputs == []:
                    # logger.warning(f"Not creating a talent for this route as it is missing either an input or outputs")
                    continue
                local_device = self.get_device_by_unique_id(videoroute.videoinput.device.unique_id)
                if local_device:
                    # Check if there is a talent for this route, and check if we have created a talent for this route
                    if videoroute.talent is None and videoroute.id not in [created_route.id for created_route in self.request_id_dict.values()]:
                        # We need to create a talent for this route
                        logger.info(f"Creating a talent for this route {videoroute.name}")
                        # self.talents_to_be_added.append(route)
                        request_id = f"CR{self._get_request_id()}"
                        self._ws_send(stream='talentstream',
                                      payload={'action': 'create',
                                               'data': {'name': videoroute.name,
                                                        'description': f"Created from route: {videoroute.name}",
                                                        'device': videoroute.videoinput.device.id},
                                               'device': videoroute.videoinput.device.id,
                                               'request_id': request_id})
                        # Store which route talent is being created
                        self.request_id_dict[request_id] = videoroute
                    else:
                        # We need to make sure our routing table matches the web
                        self.routes[videoroute.id] = videoroute
                        if videoroute.talent.id not in self.subscriptions['talentstream']:
                            # We also need to subscribe to the talent for the route
                            self._subscribe_instance(stream='talentstream', item_id=videoroute.talent.id)
                            self._ws_send(stream="talentstream",
                                          payload={"action": "patch",
                                                   "data": {'set_on': False,
                                                            'set_off': False,
                                                            'in_transition': False},
                                                   "pk": videoroute.talent.id,
                                                   "request_id": self._get_request_id()})
            return

        if ws_action == "update" or ws_action == "create" or ws_action == "patch":
            logger.debug(f"Got videoroute update: {ws_payload}")
            # if 'data' not in ws_payload:
            #     # This happens on monitor events -- don't know why
            #     videoroute = VideoRoute(ws_payload)
            # else:
            videoroute = VideoRoute(ws_payload['data'])
            if videoroute.videoinput is None or videoroute.videooutputs == []:
                # logger.warning(f"Not creating a talent for this route as it is missing either an input or outputs")
                return
            local_device = self.get_device_by_unique_id(videoroute.videoinput.device.unique_id)
            if local_device:
                # Check if there is a talent for this route, and check if we have created a talent for this route
                if videoroute.talent is None:
                    if videoroute.id not in [videoroute.id for videoroute in self.request_id_dict.values()]:
                        # We need to create a talent for this route
                        logger.info(f"Creating a talent for this route {videoroute.name}")
                        # self.talents_to_be_added.append(route)
                        request_id = f"CR{self._get_request_id()}"
                        self._ws_send(stream='talentstream', payload={'action': 'create',
                                                                      'data': {'name': videoroute.name,
                                                                               'description': f"Created from route: {videoroute.name}",
                                                                               'device': videoroute.videoinput.device.id},
                                                                      # 'device': videoroute.videoinput.device.id,
                                                                      'request_id': request_id})
                        # Store which route talent is being created
                        self.request_id_dict[request_id] = videoroute

                else:
                    # We need to make sure our routing table matches the web
                    self.routes[videoroute.id] = videoroute
                    if videoroute.talent.id not in self.subscriptions['talentstream']:
                        # We also need to subscribe to the talent for the route
                        self._subscribe_instance(stream='talentstream', item_id=videoroute.talent.id)
                        self._ws_send(stream="talentstream",
                                      payload={"action": "patch",
                                               "data": {'set_on': False,
                                                        'set_off': False,
                                                        'in_transition': False},
                                               "pk": videoroute.talent.id,
                                               "request_id": self._get_request_id()})
            return

        if ws_action == "retrieve":
            route = VideoRoute(ws_payload['data'])
            # We need to update the route with the talent which call retrieve
            self.routes[route.id] = route
            # logger.info(f"Current routes: {self.routes}")
            return

        if ws_action == "delete":
            # logging.critical(f"We need to handle route delete")
            # We need to delete the talent for this route
            delete_routes = []
            # if 'data' not in ws_payload:
            #     # This happens on monitor events -- don't know why
            #     route_id = ws_payload['id']
            # else:
            route_id = ws_payload['data']['id']
            if route_id in self.routes:
                self._ws_send(stream="talentstream",
                              payload={"action": "delete",
                                       "pk": self.routes[route_id].talent.id,
                                       "request_id": f"T{self.routes[route_id].talent.id}"})
                delete_routes.append(route_id)
            for route_id in delete_routes:
                self.routes.pop(route_id)
            return

        logger.critical(f"Unhandled message!!  VideoRouteStream {ws_action} {ws_payload}")

    def on_driverconfigstream(self, ws_payload, ws_action):

        if ws_action == 'subscribe_instance':
            return

        if ws_action == 'list':
            all_configs_list = ws_payload['data']
            # Since we get all configs for this driver user,
            #  we need to check that the driver_id matches, incase they are using this driver user for more than one driver
            config_list = [config for config in all_configs_list if config['driver'] == self.driverbaseconf.driver_id]
            # This is the intial connection, we have listed our configs
            # We need to compare the current devices we have to the config and add or remove as neccessary

            # if the list is empty create the new configs we need
            if config_list == []:
                logger.debug(f"Creating deviceconf devices")
                self._ws_send(stream='driverconfigstream',
                              payload={'action': 'create',
                                       'data': {'driver': self.driverbaseconf.driver_id,
                                                'key': 'devices'},
                                       'driver': self.driverbaseconf.uuid,
                                       'request_id': self._get_request_id()})
                return
            for config in config_list:
                my_key = config['key']
                logger.debug(f"Got key: {my_key}")
                if my_key not in ['devices']:
                    logger.error(f"Got unknown config key: {my_key}")
                    return

                # store driverconfig id
                self.driverbaseconf.driverconfig_id = config['id']

                if config['id'] not in self.subscriptions['driverconfigstream']:
                    # We need to sub to this driverconfig to get updates
                    self._subscribe_instance(stream="driverconfigstream", item_id=config['id'])

                self._process_config(config)
            return

        if ws_action == "update":
            config = ws_payload['data']
            logger.debug(f"Got config update")
            self._process_config(config)
            return

        if ws_action == 'create':
            config = ws_payload['data']
            self.driverbaseconf.driverconfig_id = config['id']
            logger.critical(f"Driver Config has been created")
            self._subscribe_instance(stream='driverconfigstream', item_id=config['id'])
            self.on_driverconfig_set(config)  # for macrodriver
            self._process_config(config)
            return

        if ws_action == 'retrieve':
            # The only time we retrieve is when a talent has changed names
            logging.debug(f"In driverconfig retrieve -- a talent MUST have had it's name changed")
            config = ws_payload['data']
            try:
                configurations = json.loads(config['config_value'])
                if configurations is None:
                    configurations = []

            except json.JSONDecodeError as error:
                logger.critical(f"Unable to read config file: {error}")
                return
            for config_device in configurations:
                local_device = self.get_device_by_unique_id(unique_id=config_device['unique_id'])
                for talent in local_device.talents:
                    if hasattr(talent, 'old_name'):
                        if talent.name != talent.old_name:
                            logger.debug(f"Found name changed from {talent.old_name} to {talent.name} ")
                            for config_talent in config_device['talents']:
                                if config_talent['name'] == talent.old_name:
                                    config_talent['name'] = talent.name
            self.update_driverconfig(configurations)
            return

        logger.critical(f"Unhandled message!!  DriverConfigStream {ws_action} {ws_payload}")

    def on_devicestream(self, ws_payload, ws_action):

        if ws_action == 'subscribe_instance':
            return

        if ws_action == 'list':
            # We only list if we suspect devices on the server already exist
            # we need to go through the devices in the list make sure they are ours and update our id's
            for device in [Device(d) for d in ws_payload['data']]:
                if device.driver.driver_uuid == self.driverbaseconf.uuid:
                    local_device = self.get_device_by_unique_id(device.unique_id)
                    if local_device:
                        local_device.id = device.id
                    else:
                        self.driverbaseconf.devices.append(device)
                    dispatcher.send(signal="Save Shelve", key='devices', data=self.driverbaseconf.devices)
            return

        if ws_action == 'create':
            if ws_payload['errors'] == [{'unique_id': ['device with this unique id already exists.']}]:
                # This means our local device already exists on the server, however we do not have the device id
                # We need to list devices to update any existing ID's
                self._ws_send(stream="devicestream", payload={"action": "list", "request_id": self._get_request_id()})
                return
            device = Device(ws_payload['data'])
            # We need to store the device.id
            local_device = self.get_device_by_unique_id(unique_id=device.unique_id)
            if local_device:
                local_device.id = device.id
            else:
                self.driverbaseconf.devices.append(device)
            dispatcher.send(signal="Save Shelve", key='devices', data=self.driverbaseconf.devices)

            # Subscribe to device
            self._subscribe_instance(stream='devicestream', item_id=device.id)
            # If we create a new device, we need to create its talents
            for talent in local_device.talents:
                self._create_talent(name=talent.name,
                                    description=talent.description,
                                    state=talent.state,
                                    is_range=talent.is_range,
                                    range_min=talent.range_min,
                                    range_max=talent.range_max,
                                    disable_if_in_transition=talent.disable_if_in_transition,
                                    device_id=local_device.id)

            for videoinput in local_device.videoinputs:
                if not self.videoroute_monitoring:
                    self.videoroute_monitoring = True
                    self._ws_send(stream='videoroutestream', payload={"action": "monitor", "data": {}, "request_id": self._get_request_id()})
                self._create_videoinput(name=videoinput.name,
                                        description=f'Input -- {local_device.name}-- {local_device.unique_id}',
                                        number=videoinput.number,
                                        device_id=local_device.id)

            for videooutput in local_device.videooutputs:
                if not self.videoroute_monitoring:
                    self.videoroute_monitoring = True
                    self._ws_send(stream='videoroutestream', payload={"action": "monitor", "data": {}, "request_id": self._get_request_id()})
                self._create_videooutput(name=videooutput.name,
                                         description=f'Output -- {local_device.name} -- {local_device.unique_id}',
                                         number=videooutput.number,
                                         device_id=local_device.id)
            return

        if ws_action == 'retrieve':
            if ws_payload['response_status'] == 404:
                # We have not found what we tried to retrieve
                # On retrieve I'm using the pk as the request_id
                # This means the device has been deleted / or no longer exists
                for_removal = None
                for device in self.driverbaseconf.devices:
                    if device.id == ws_payload['request_id']:
                        for_removal = self.driverbaseconf.devices.index(device)
                        logger.critical(f"Removing device from local_devices: {device}  id: {ws_payload['request_id']}")
                if for_removal is not None:
                    self.driverbaseconf.devices.pop(for_removal)
                dispatcher.send(signal="Save Shelve", key='devices', data=self.driverbaseconf.devices)
                return

            device = Device(ws_payload['data'])
            # Verify the driver we retrieved unique_id match's the id we got
            local_device = self.get_device_by_unique_id(unique_id=device.unique_id)
            if not local_device:
                logger.critical(f"Unable to find local stored device on server: {device}")
                return
            if local_device.id != device.id:
                logger.critical(f"Our local id doesn't match the id on the server: {device}")
                return
            # Update to current state
            self._ws_send(stream='devicestream',
                          payload={'action': 'patch',
                                   'data': {'is_online': local_device.online},
                                   'pk': device.id,
                                   'request_id': self._get_request_id()})
            # Subscribe
            self._subscribe_instance(stream='devicestream', item_id=device.id)

            # Retrieve or create all the devices talents
            for talent in local_device.talents:
                if talent.id is None:
                    self._create_talent(name=talent.name,
                                        description=talent.description,
                                        state=talent.state,
                                        is_range=talent.is_range,
                                        range_min=talent.range_min,
                                        range_max=talent.range_max,
                                        disable_if_in_transition=talent.disable_if_in_transition,
                                        device_id=local_device.id)
                else:
                    self._ws_send(stream='talentstream', payload={"action": "retrieve", 'pk': talent.id, "request_id": talent.id})

            # Retrieve or create all the devices videoinputs / videooutputs
            for videoinput in local_device.videoinputs:
                if not self.videoroute_monitoring:
                    self.videoroute_monitoring = True
                    self._ws_send(stream='videoroutestream', payload={"action": "monitor", "data": {}, "request_id": self._get_request_id()})
                if videoinput.id is None:
                    self._create_videoinput(name=videoinput.name,
                                            description=f'Input -- {local_device.name}-- {local_device.unique_id}',
                                            number=0,
                                            device_id=local_device.id)
                else:
                    self._ws_send(stream='videoinputstream', payload={"action": "retrieve", 'pk': videoinput.id, "request_id": videoinput.id})

            for videooutput in local_device.videooutputs:
                if not self.videoroute_monitoring:
                    self.videoroute_monitoring = True
                    self._ws_send(stream='videoroutestream', payload={"action": "monitor", "data": {}, "request_id": self._get_request_id()})
                if videooutput.id is None:
                    self._create_videooutput(name=videooutput.name,
                                             description=f'Output -- {local_device.name} -- {local_device.unique_id}',
                                             number=0,
                                             device_id=local_device.id)
                else:
                    self._ws_send(stream='videooutputstream', payload={"action": "retrieve", 'pk': videooutput.id, "request_id": videooutput.id})
            return

        if ws_action == 'update' or ws_action == 'patch':
            # Just need to check if we had errors finding the device
            if ws_payload['response_status'] == 404 and ws_payload['errors'] == ['Not found']:
                # We need to remove this device from our list
                # On patches we use the device ID as request_id
                self._remove_local_device_by_id(id=ws_payload['request_id'])
                return
            device = Device(ws_payload['data'])
            # Update online to match
            local_device = self.get_device_by_unique_id(unique_id=device.unique_id)
            if not local_device:
                logger.critical(f"Got {ws_action} for unknown device {ws_payload}")
            else:
                if local_device.online != ws_payload['data']['is_online']:
                    local_device.online = ws_payload['data']['is_online']
                    dispatcher.send(signal="Save Shelve", key='devices', data=self.driverbaseconf.devices)
            return

        if ws_action == 'delete':
            if ws_payload['response_status'] == 404:
                logger.critical(f"Delete failed")
                return
            # There are two ways we could end up in delete
            # Someone has delete something on the web
            # We have delete something via ws
            # If it is done on the web get the this in data {"pk": 1}
            # If we do it data is None but I've put the pk in the request id
            if ws_payload['data'] is None:
                self._remove_local_device_by_id(id=ws_payload['request_id'])
            else:
                self._remove_local_device_by_id(id=ws_payload['data']['pk'])
            return

        logger.critical(f"Unhandled message!!  DeviceStream {ws_action} {ws_payload}")

    def on_talentstream(self, ws_payload, ws_action):
        # if ws_action == 'list':
        #     pass
        #     return

        if ws_action == 'subscribe_instance':
            return

        if ws_action == "create":
            talent = Talent(ws_payload['data'])
            logging.debug(f"create talent: {talent}")

            # store talent id
            local_device = self.get_device_by_unique_id(unique_id=talent.device.unique_id)
            if ws_payload['request_id'] in self.request_id_dict.keys():
                # This must be a route...
                talent.is_a_route = True
                # We need to add the talent to the route..
                # We also need to update the device with the talent
                local_device.talents.append(talent)
                dispatcher.send(signal="Save Shelve", key='devices', data=self.driverbaseconf.devices)
                # Make sure we are subscribed
                self._subscribe_instance(stream='talentstream', item_id=talent.id)
                # We need to make sure the locally stored route is updated with the talent
                videoroute = self.request_id_dict[ws_payload['request_id']]
                # patch the web route
                self._ws_send(stream='videoroutestream',
                              payload={'action': 'patch',
                                       'data': {},
                                       'talent': talent.id,
                                       'pk': videoroute.id,
                                       "request_id": self._get_request_id()})
                self._ws_send(stream='videoroutestream', payload={"action": "retrieve", "pk": videoroute.id, "request_id": videoroute.id})
                return

            if local_device:
                local_talent = local_device.get_talent_by_name(name=talent.name)
                local_talent.id = talent.id
                dispatcher.send(signal="Save Shelve", key='devices', data=self.driverbaseconf.devices)

                logger.info(f"Subscribing to talent from talent list {talent.id}")
                self._subscribe_instance(stream='talentstream', item_id=talent.id)
            return

        if ws_action == 'retrieve':
            if ws_payload['response_status'] == 404:
                # We have not found what we tried to retrieve
                # On retrieve I'm using the pk as the request_id
                # This means the talent has been deleted / or no longer exists
                self._remove_local_talent_by_id(id=ws_payload['request_id'])
                return

            talent = Talent(ws_payload['data'])
            local_talent = self.get_local_talent(talent)
            if local_talent:
                local_talent.state, local_talent.requested_range_value, local_talent.range_value = (talent.state, talent.requested_range_value, talent.range_value)
            logger.info(f"Subscribing to talent from talent list {talent.id}")
            self._subscribe_instance(stream='talentstream', item_id=talent.id)
            self._ws_send(stream="talentstream",
                          payload={"action": "patch",
                                   "data": {'set_on': False,
                                            'set_off': False,
                                            'in_transition': False},
                                   "pk": talent.id,
                                   "request_id": self._get_request_id()})

            return

        if ws_action == "update" or ws_action == "patch":
            talent = Talent(ws_payload['data'])
            local_talent = self.get_local_talent(talent)
            if local_talent:
                local_talent.state, local_talent.requested_range_value, local_talent.range_value = (talent.state, talent.requested_range_value, talent.range_value)
                if local_talent.name != talent.name:
                    logger.info(f"Talent name change detected: {local_talent.name} to {talent.name}")
                    # Retrieve the config file to update it
                    local_talent.old_name = local_talent.name
                    local_talent.name = talent.name
                    self._ws_send(stream="driverconfigstream",
                                  payload={"action": "retrieve",
                                           "pk": self.driverbaseconf.driverconfig_id,
                                           "request_id": self._get_request_id()})
            # Only pass on things the driver needs to react to
            if talent.set_on or talent.set_off or talent.range_value != talent.requested_range_value:
                self.on_talent_change(talent)

            return

        if ws_action == "delete":
            if ws_payload['response_status'] == 404:
                logger.critical(f"Delete failed")
                return
            # There are two ways we could end up in delete
            # Someone has delete something on the web
            # We have delete something via ws
            # If it is done on the web get the this in data {"pk": 1}
            # If we do it data is None but I've put the pk in the request id
            if ws_payload['data'] is None:
                self._remove_local_talent_by_id(id=ws_payload['request_id'])
            else:
                self._remove_local_talent_by_id(id=ws_payload['data']['pk'])
            return

        logger.critical(f"Unhandled message!!  TalentStream {ws_action} {ws_payload}")

    def on_videoinputstream(self, ws_payload, ws_action):
        # if ws_action == 'list':
        #     pass
        #     return

        if ws_action == 'subscribe_instance':
            return

        if ws_action in ["create", 'retrieve', 'update', 'patch']:
            videoinput = VideoInput(ws_payload['data'])
            if not self.videoroute_monitoring:
                self.videoroute_monitoring = True
                self._ws_send(stream='videoroutestream', payload={"action": "monitor", "data": {}, "request_id": self._get_request_id()})
            # store videoinput id and number
            local_device = self.get_device_by_unique_id(unique_id=videoinput.device.unique_id)
            if not local_device:
                logger.critical(f"Unable to get device by unique_id {local_device}")
                return
            local_videoinput = local_device.get_videoinput_by_name(name=videoinput.name)
            if not local_videoinput:
                logger.critical(f"Unable to get videoinput by name {videoinput.name}")
                return
            if (local_videoinput.id, local_videoinput.number) != (videoinput.id, videoinput.number):
                local_videoinput.id = videoinput.id
                local_videoinput.number = videoinput.number
                dispatcher.send(signal="Save Shelve", key='devices', data=self.driverbaseconf.devices)

            if ws_action == 'create':
                self._subscribe_instance(stream='videoinputstream', item_id=videoinput.id)
            return

        if ws_action == "delete":
            if ws_payload['response_status'] == 404:
                logger.critical(f"Delete failed")
                return
            # There are two ways we could end up in delete
            # Someone has delete something on the web
            # We have delete something via ws
            # If it is done on the web get the this in data {"pk": 1}
            # If we do it data is None but I've put the pk in the request id
            if ws_payload['data'] is None:
                self._remove_local_videoinput_by_id(id=ws_payload['request_id'])
            else:
                self._remove_local_videoinput_by_id(id=ws_payload['data']['pk'])
            return

        logger.critical(f"Unhandled message!!  VideoInputStream {ws_action} {ws_payload}")

    def on_videooutputstream(self, ws_payload, ws_action):
        # if ws_action == 'list':
        #     pass
        #     return

        if ws_action == 'subscribe_instance':
            return

        if ws_action in ["create", 'retrieve', 'update', 'patch']:
            videooutput = VideoOutput(ws_payload['data'])
            logging.debug(f"{ws_action} videooutput: {videooutput}")
            if not self.videoroute_monitoring:
                self.videoroute_monitoring = True
                self._ws_send(stream='videoroutestream', payload={"action": "monitor", "data": {}, "request_id": self._get_request_id()})

            # store videooutput id
            local_device = self.get_device_by_unique_id(unique_id=videooutput.device.unique_id)
            if not local_device:
                logger.critical(f"Unable to get device by unique_id {local_device}")
                return
            local_videooutput = local_device.get_videooutput_by_name(name=videooutput.name)
            if not local_videooutput:
                logger.critical(f"Unable to get videooutput by name {videooutput.name}")
                return
            if (local_videooutput.id, local_videooutput.number) != (videooutput.id, videooutput.number):
                local_videooutput.id = videooutput.id
                local_videooutput.number = videooutput.number
                dispatcher.send(signal="Save Shelve", key='devices', data=self.driverbaseconf.devices)
            if ws_action == 'create':
                self._subscribe_instance(stream='videooutputstream', item_id=videooutput.id)
            return

        if ws_action == "delete":
            if ws_payload['response_status'] == 404:
                logger.critical(f"Delete failed")
                return
            # There are two ways we could end up in delete
            # Someone has delete something on the web
            # We have delete something via ws
            # If it is done on the web get the this in data {"pk": 1}
            # If we do it data is None but I've put the pk in the request id
            if ws_payload['data'] is None:
                self._remove_local_videooutput_by_id(id=ws_payload['request_id'])
            else:
                self._remove_local_videooutput_by_id(id=ws_payload['data']['pk'])
            return

        logger.critical(f"Unhandled message!!  VideoOutputStream {ws_action} {ws_payload}")

    def add_remove_unique_id_devices(self, local_devices, web_devices):
        add_devices = [w for w in web_devices if w.unique_id not in [ld.unique_id for ld in local_devices]]
        remove_devices = [ld for ld in local_devices if ld.unique_id not in [w.unique_id for w in web_devices]]

        for device in add_devices:
            logging.debug(f"Adding device: {device}")
            # Do not create device here
            # Will be created on retrieve
            if device.unique_id not in [d.unique_id for d in local_devices]:
                local_devices.append(device)
            else:
                logger.critical(f"For some reason we are trying to add a device to local devices twice!")
        for device in remove_devices:
            logging.debug(f"Removing device: {device}")
            local_devices.pop(local_devices.index(device))
            if device.id:
                self._ws_send(stream="devicestream",
                              payload={"action": "delete",
                                       "pk": device.id,
                                       "request_id": device.id})

    def add_or_remove_talents(self, local_device, web_device):
        add_talents = [w for w in web_device.talents if w.name not in [ld.name for ld in local_device.talents]]
        remove_talents = [ld for ld in local_device.talents if ld.name not in [w.name for w in web_device.talents]]

        for talent in add_talents:
            logging.debug(f"Adding talent: {talent}")
            # Do not create talent here
            # will be created on device retrieve
            local_device.talents.append(talent)
        for talent in remove_talents:
            if talent.is_a_route:
                # Since routes are not in the webconfig -- don't delete them here
                # They will be deleted when the route is deleted
                # There is a chance that a route talent will be orphaned here
                continue
            logging.debug(f"Removing talent: {talent}")
            local_device.talents.pop(local_device.talents.index(talent))
            if talent.id:
                self._ws_send(stream="talentstream",
                              payload={"action": "delete",
                                       "pk": talent.id,
                                       "request_id": talent.id})

    def sync_local_devices_to_web_devices(self, local_devices, web_devices):
        # Now we need to update parameters in local devices
        for web_device in web_devices:
            for local_device in local_devices:
                if local_device.unique_id == web_device.unique_id:
                    # logging.debug(f"{dataclasses.asdict(web_device)}")
                    changed_keys = []
                    added_keys = [key for key in web_device.__dict__ if key not in local_device.__dict__]
                    if added_keys:
                        logging.debug(f"Found added keys: {added_keys}")
                    for key in added_keys:
                        logging.debug(f"Adding key: {key} {getattr(web_device, key)} to {local_device.name}")
                        setattr(local_device, key, getattr(web_device, key))
                    changed_keys = [key for key in web_device.__dict__ if key in local_device.__dict__ and web_device.__dict__[key] != local_device.__dict__[key]]
                    if changed_keys:
                        logging.debug(f"Found changed keys: {changed_keys}")
                    for key in changed_keys:
                        if key not in ['id', 'is_online', 'videoinputs', 'videooutputs', 'talents']:
                            if getattr(local_device, key) != getattr(web_device, key):
                                # Update this key
                                logging.debug(f"Updating key: {key} {getattr(local_device, key)} to {getattr(web_device, key)}")
                                setattr(local_device, key, getattr(web_device, key))
                            else:
                                logging.debug(f"Skipping key: {key}")
                        else:
                            logging.debug(f"Skipping state key: {key} local:{getattr(local_device, key)} to web:{getattr(web_device, key)} ")

                    # if (local_device.name, local_device.description) != (web_device.name, web_device.description):
                    #     logging.debug(f"Found unmatched: {(local_device.name, local_device.description)} {web_device.name, web_device.description}")
                    #     # Update local device but not talent?
                    #     for key in ['name', 'description']:
                    #         if getattr(web_device, key) != getattr(local_device, key):
                    #             logging.debug(f"Updating {key} to {getattr(web_device, key)}")
                    #             setattr(local_device, key, getattr(web_device, key))

                    # First make sure we have the same number of talents
                    self.add_or_remove_talents(local_device, web_device)
                    for talent in web_device.talents:
                        for local_talent in local_device.talents:
                            if local_talent.name == talent.name:
                                changed_keys = []
                                # We need to check if the config has changed, and we need to check all values (with the exception of state values), even extra values that the user has added
                                # We do this by changing the talent object into a dict, and comare keys, ignoring the state keys
                                added_keys = [key for key in talent.__dict__ if key not in local_talent.__dict__]
                                if added_keys:
                                    logging.debug(f"Found added keys: {added_keys}")
                                for key in added_keys:
                                    logging.debug(f"Adding key: {key} {getattr(talent, key)} to {local_talent.name}")
                                    setattr(local_talent, key, getattr(talent, key))
                                changed_keys = [key for key in talent.__dict__ if key in local_talent.__dict__ and talent.__dict__[key] != local_talent.__dict__[key]]
                                if changed_keys:
                                    logging.debug(f"Found changed keys: {changed_keys}")
                                for key in changed_keys:
                                    if key not in ['id', 'state', 'range_value', 'requested_range_value', 'is_a_route']:
                                        if getattr(local_talent, key) != getattr(talent, key):
                                            # Update this key
                                            logging.debug(f"Updating key: {key} {getattr(local_talent, key)} to {getattr(talent, key)}")
                                            setattr(local_talent, key, getattr(talent, key))
                                        else:
                                            logging.debug(f"Skipping key: {key}")
                                    else:
                                        logging.debug(f"Skipping state key: {key} local:{getattr(local_talent, key)} to web:{getattr(talent, key)} ")

    def _process_config(self, config):
        try:
            if config['config_value'] == '':
                configurations = []
            else:
                configurations = json.loads(config['config_value'])

            web_device_configurations = [Device(conf) for conf in configurations]

        except json.JSONDecodeError as error:
            logger.critical(f"Unable to read config file: {error}")
            return

        # Make sure self.driverbaseconf.devices has the same devices as the web_device_configurations
        self.add_remove_unique_id_devices(self.driverbaseconf.devices, web_device_configurations)

        # Syncronize/update self.driverbaseconf.devices from web_device_configurations
        self.sync_local_devices_to_web_devices(self.driverbaseconf.devices, web_device_configurations)

        # save to shelve
        dispatcher.send(signal="Save Shelve", key='devices', data=self.driverbaseconf.devices)

        # We need to retrieve all devices we have id's for, and create any devices we don't have
        for device in self.driverbaseconf.devices:
            logger.debug(f"checking device: {device} id: {device.id}")
            if device.id:
                # Use device.id as request_id, so we can handle 404's
                self._ws_send(stream='devicestream', payload={"action": "retrieve", 'pk': device.id, "request_id": device.id})
            else:
                # There is a risk that the web_config is updated too quickly -- we may attempt to create duplicate devices
                self._create_device(name=device.name,
                                    description=device.description,
                                    online=device.online,
                                    unique_id=device.unique_id)

        # We need to get the inital list of videoroutes
        for device in self.driverbaseconf.devices:
            if device.videoinputs != [] or device.videooutputs != []:
                # Get all videoroutes
                self._ws_send(stream='videoroutestream', payload={"action": "list", "request_id": self._get_request_id()})
                self._ws_send(stream='videoroutestream', payload={"action": "monitor", "data": {}, "request_id": self._get_request_id()})
                self.videoroute_monitoring = True
                break

        # Pass the yaml config to the user
        self.on_updated_driverconfig(configurations)

        # except Exception as error:
        #     logger.critical(f"{error} {config}")

    def update_driverconfig(self, config):
        try:
            device_configurations = json.dumps(config, indent=4)
        except json.JSONDecodeError as error:
            logger.critical(f"Unable to read config file: {error}")
            return

        self._ws_send(stream="driverconfigstream",
                      payload={"action": "patch",
                               "data": {"config_value": device_configurations},
                               "pk": self.driverbaseconf.driverconfig_id,
                               "request_id": self._get_request_id()})

    def _subscribe_instance(self, stream, item_id):
        if item_id not in self.subscriptions[stream]:
            logger.debug(f"Subing: {stream} {item_id}")
            self.subscriptions[stream].add(item_id)
            self._ws_send(stream=stream,
                          payload={"action": "subscribe_instance",
                                   "pk": item_id,
                                   "request_id": self._get_request_id()})
        else:
            logger.debug(f"Skipping subing: {stream} {item_id} DUPLICATE")

    def _create_device(self, name, description, online, unique_id):
        logger.debug(f"Creating device {name}")
        self._ws_send(stream='devicestream',
                      payload={'action': 'create',
                               'data': {'name': name,
                                        'description': description,
                                        'unique_id': unique_id,
                                        'is_online': online,
                                        'talents': []},
                               'driver': self.driverbaseconf.uuid,
                               'request_id': self._get_request_id()})

    def _create_talent(self, name, description, state, device_id, request_id=None, is_range=False, range_min=0, range_max=0, disable_if_in_transition=False):
        logger.debug(f"Creating talent {name}")
        if not request_id:
            request_id = self._get_request_id()
        self._ws_send(stream='talentstream',
                      payload={'action': 'create',
                               'data': {'name': name,
                                        'description': description,
                                        'state': state,
                                        'is_range': is_range,
                                        'range_min': range_min,
                                        'range_max': range_max,
                                        'disable_if_in_transition': disable_if_in_transition},
                               'device': device_id,
                               'request_id': request_id})

    def _create_videoinput(self, name, description, number, device_id, request_id=None):
        logger.debug(f"Creating videoinput {name}")
        if not request_id:
            request_id = self._get_request_id()
        if not self.videoroute_monitoring:
            self.videoroute_monitoring = True
            self._ws_send(stream='videoroutestream',
                          payload={"action": "monitor",
                                   "data": {},
                                   "request_id": self._get_request_id()})
        self._ws_send(stream='videoinputstream',
                      payload={'action': 'create',
                               'data': {'name': name,
                                        'description': description,
                                        'number': number,
                                        'videoroutes': []},
                               'device': device_id,
                               'request_id': request_id})

    def _create_videooutput(self, name, description, number, device_id, request_id=None):
        logger.debug(f"Creating videooutput {name}")
        if not request_id:
            request_id = self._get_request_id()
        if not self.videoroute_monitoring:
            self.videoroute_monitoring = True
            self._ws_send(stream='videoroutestream',
                          payload={"action": "monitor",
                                   "data": {},
                                   "request_id": self._get_request_id()})
        self._ws_send(stream='videooutputstream',
                      payload={'action': 'create',
                               'data': {'name': name,
                                        'description': description,
                                        'number': number,
                                        'videoroutes': []},
                               'device': device_id,
                               'request_id': request_id})

    def get_device_by_unique_id(self, unique_id):
        for device in self.driverbaseconf.devices:
            if device.unique_id == unique_id:
                return device
        return None

    def _remove_local_device_by_id(self, id):
        remove_indexes = []
        if type(id) == str:
            id = int(id.replace("T", ""))
        for local_device in self.driverbaseconf.devices:
            if local_device.id == id:
                remove_indexes.append(self.driverbaseconf.devices.index(local_device))

        for item in remove_indexes:
            self.driverbaseconf.devices.pop(item)
            logger.critical(f"Removing device from local_devices: {item}  id: {id}")
            dispatcher.send(signal="Save Shelve", key='devices', data=self.driverbaseconf.devices)

    def get_local_talent(self, talent):
        local_device = self.get_device_by_unique_id(unique_id=talent.device.unique_id)
        return local_device.get_talent_by_id(id=talent.id)

    def _remove_local_talent_by_id(self, id):
        # Go through all local devices
        for device in self.driverbaseconf.devices:
            local_talent = device.get_talent_by_id(id)
            if local_talent:
                logger.debug(f"Removing talent by ID {local_talent}")
                device.talents.pop(device.talents.index(local_talent))
                dispatcher.send(signal="Save Shelve", key='devices', data=self.driverbaseconf.devices)

    def _remove_local_videoinput_by_id(self, id):
        # Go through all local devices
        for device in self.driverbaseconf.devices:
            local_videoinput = device.get_videoinput_by_id(id)
            if local_videoinput:
                logger.debug(f"Removing talent by ID {local_videoinput}")
                device.videoinput.pop(device.videoinput.index(local_videoinput))
                dispatcher.send(signal="Save Shelve", key='devices', data=self.driverbaseconf.devices)

    def _remove_local_videooutput_by_id(self, id):
        # Go through all local devices
        for device in self.driverbaseconf.devices:
            local_videooutput = device.get_videooutput_by_id(id)
            if local_videooutput:
                logger.debug(f"Removing talent by ID {local_videooutput}")
                device.videooutput.pop(device.videooutput.index(local_videooutput))
                dispatcher.send(signal="Save Shelve", key='devices', data=self.driverbaseconf.devices)

    def _ws_status(self, status):
        logger.info(f"ws status: {status}")
        if status['connected']:
            logger.debug('setting connected to True')
            self.ws_connected = True
            if self.registration.authorized:
                self._ws_send(stream='driverconfigstream',
                              payload={'action': 'list',
                                       'request_id': self._get_request_id()})

        else:
            logger.warning('setting connected to False')
            self.ws_connected = False
            for sub in self.subscriptions:
                self.subscriptions[sub] = set()
            self.request_id_dict = {}
            self.routes = {}

    def run(self):
        """Run Worker Thread."""
        self._start_registration()
        while not self.shutdown and not self.registration.authorized:
            logger.info('waiting for authorization...')
            time.sleep(5)
            continue
        else:
            # We need to retrive our config
            self._ws_send(stream='driverconfigstream',
                          payload={'action': 'list',
                                   'request_id': self._get_request_id()})

        # count = 0
        while not self.shutdown:
            self.periodic()
            time.sleep(1)

    def periodic(self):
        pass

    def _ws_send(self, stream, payload):
        if self.ws_connected and self.registration.authorized:
            dispatcher.send("Websocket Send", sender=self, stream=stream, payload=payload)
        else:
            if not self.shutdown:
                logger.error('Unable to send when disconnected or not yet authorized')
