from dataclasses import dataclass
from cryptography.fernet import Fernet

ENCRYPTION_KEY = b'kjSMVBkuTWZXTJ0mLCZFHddMhFGfmlsAw32tms73sDA='
cipher_suite = Fernet(ENCRYPTION_KEY)


@dataclass
class DriverBaseConfig:
    driver_name = 'DriverBase'
    uuid = None
    driver_id = None
    demo = False
    server_url = 'http://localhost:8000'
    token: str = 'gAAAAABfOz56Kf6pvHE2iFVM2rEarygqMK8wpdqwggtspyCjq4H3DgEP2jGSFtkW3cBpSdaZkgOw4M7pO4MKUfqAMuKt3XhK8UaF-KxbznG8XYu4nwWxm26hL-asbp_z5p0MnMeH23KX'  # P@55w0rd

    devices = []

    def set_token(self, password):
        self.token = cipher_suite.encrypt(password.encode()).decode()

    def get_token(self):
        try:
            return cipher_suite.decrypt(self.token.encode()).decode()
        except Exception as error:
            print(error)
            return ''
